package main

import "fmt"

func main() {
	var x int8 = -25
	var ch int32 = 'c'
	//var y float32 = 5.0
	fmt.Printf("%b\n", x)
	fmt.Println(x)
	fmt.Println(5/4.0)
	//fmt.Println(x / y)
	fmt.Printf("%d %[1]c %[1]q", ch)
}
