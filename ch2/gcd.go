package main

import "fmt"

func main() {
	fmt.Println(gcd(35,42))
}

func gcd(x int, y int) int {
	for y != 0 {
		x,y = y, x % y
	}
	return x
}
