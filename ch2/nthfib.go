package main

import "fmt"

func nFib(n int) int {
	var x, y int = 0, 1
	for i := 0;i < n;i++ {
		x, y = y, x+y
	}
	return x
}

func main() {
	fmt.Println(nFib(5))
	fmt.Println(nFib(10))
}
