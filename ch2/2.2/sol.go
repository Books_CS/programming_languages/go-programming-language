package main

import (
	"fmt"
	"os"
	"strconv"
)

type Celsius float64
type Fahrenheit float64
type Meter float64
type Feet float64
type Kilogram float64
type Pound float64

func CToF(c Celsius) Fahrenheit { return Fahrenheit(c*9/5 + 32) }
func FToC(f Fahrenheit) Celsius { return Celsius((f - 32) * 5 / 9) }
func MToF(m Meter) Feet         { return Feet(m * 3.2808) }
func FToM(f Feet) Meter         { return Meter(f * 0.3048) }
func KToP(k Kilogram) Pound     { return Pound(k * 2.2046) }
func PToK(p Pound) Kilogram     { return Kilogram(p / 2.2046) }

func (c Celsius) String() string    { return fmt.Sprintf("%gC", c) }
func (f Fahrenheit) String() string { return fmt.Sprintf("%gF", f) }
func (m Meter) String() string      { return fmt.Sprintf("%gM", m) }
func (f Feet) String() string       { return fmt.Sprintf("%gF", f) }
func (k Kilogram) String() string   { return fmt.Sprintf("%gK", k) }
func (p Pound) String() string      { return fmt.Sprintf("%gP", p) }

func main() {
	if len(os.Args) > 1 {
		for _, arg := range os.Args[1:] {
			t, err := strconv.ParseFloat(arg, 64)
			if err != nil {
				fmt.Fprintf(os.Stderr, "cf: %v\n", err)
				os.Exit(1)
			}
			fahrenheit := Fahrenheit(t)
			celsius := Celsius(t)
			meter := Meter(t)
			feet := Feet(t)
			kilogram := Kilogram(t)
			pound := Pound(t)
			fmt.Printf("%s = %s, %s = %s\n%s = %s, %s = %s\n%s = %s, %s = %s\n", fahrenheit, FToC(fahrenheit), celsius, CToF(celsius), feet, FToM(feet), meter, MToF(meter), kilogram, KToP(kilogram), pound, PToK(pound))
		}
	} else {
		fmt.Println("Enter the unit : ")
		var t int
		fmt.Scanln(&t)
		fahrenheit := Fahrenheit(t)
		celsius := Celsius(t)
		meter := Meter(t)
		feet := Feet(t)
		kilogram := Kilogram(t)
		pound := Pound(t)
		fmt.Printf("%s = %s, %s = %s\n%s = %s, %s = %s\n,%s = %s, %s = %s\n", fahrenheit, FToC(fahrenheit), celsius, CToF(celsius), feet, FToM(feet), meter, MToF(meter), kilogram, KToP(kilogram), pound, PToK(pound))
	}

}
