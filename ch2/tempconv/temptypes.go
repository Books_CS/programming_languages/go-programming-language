// Package tempconv performs Celsius and Fahrenheit conversions.
package tempconv

import "fmt"

type Celsius float64
type Fahrenheit float64

const (
	AbsoluteZeroC Celsius = -273
	FreezingC Celsius = 0
	BoilingC Celsius = 100
)

func (c Celsius) String() string { return fmt.Sprintf("%gC", c) }
func (f Fahrenheit) String() string { return fmt.Sprintf("%gF", f) }

/*Excercise 2.1*/
type Kelvin float64

const AbsoluteZeroK Kelvin = 0
const FreezingK Kelvin = 273
const BoilingK Kelvin = 373

func (k Kelvin) String() string { return fmt.Sprintf("%gK", k) }
